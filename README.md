# Metrics-server kustomize example
> This is public project hosted on GitLab

# Installing on minikube
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

Bare installation causes this error 
```log
Get "https://192.168.49.2:10250/stats/summary?only_cpu_and_memory=true": x509: cannot validate certificate for 192.168.49.2 because it doesn't contain any IP SANs`
```

The error is due to the self-signed TLS certificate. So adding `--kubelet-insecure-tls` to the `components.yaml` and re-applying it to the K8s cluster fixes the issue. [Reference configuration](https://github.com/kubernetes-sigs/metrics-server#configuration). The full solution to re-generate certs you can follow on [StackOverflow](https://stackoverflow.com/questions/64767239/kubernetes-metrics-server-not-running).

# Operations
```sh
# commands reference path is `./metrics-server-official/`

clear; diff -y <(cat metric-server/components.yaml) <(kustomize build overlay/)
kustomize build overlay/ | kubectl diff  -f -
kustomize build overlay/ | kubectl apply -f - --dry-run=server

# Use build-in kubectl kustomize
kubectl kustomize overlay/
```

